# Siril-contrib-doc

This is the repo containting guidelines for contributing to [Siril](https://gitlab.com/free-astro/siril)  
The built documentation is available on [Read the Docs](https://siril-contrib-doc.readthedocs.io/en/latest/)