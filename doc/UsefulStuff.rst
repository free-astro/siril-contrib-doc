Useful stuff
-------------

Limit magnitude
===============

:download:`Modelling <./_data/LimitmagKstars.pdf>` performed on Kstars catalogues 
data, used to compute limit magnitude.

UI files parsing
================

:download:`Utility <./_tools/uiparser.py>` to parse .ui files, display the tree
in a compact way, list all the statics (for caching) and callbacks


Overlap norms
=============

:download:`Normalization <./_data/Overlaps_rev1.pdf>` on overlaps used for mosaic
stitching.