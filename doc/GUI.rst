Graphical User Interface (GUI)
------------------------------

Siril has two modes of operation: headless using **siril-cli** and with a GTK3
GUI using the usual **siril** command.

The GUI is written using XML UI files suitable for reading by GtkBuilder.

Until recently there was a single enormous GUI file but this caused problems as
it was slow to read and process at startup and very cumbersome indeed to edit
by hand: it also meant that merge requests that each made substantial GUI
changes tended to conflict horribly.

Now each GTK toplevel (each GtkDialog, GtkColorChooserDialog and GtkApplicationDialog)
has its own UI file. The exception is the menus, which are all together in a file
called :file:`menus.ui`. These live in :file:`src/gui/uifiles`. If you are modifying a part
of the GUI, now you only risk conflicts with anyone else working on the same
part of the GUI rather than any of it. There might still be issues around the
Preferences dialog, but the problem should be much reduced.

If you add a new UI file (you should do so if you add another GtkDialog to
provide a GUI for a new tool, for example) you must add it in :file:`src/gui/uifiles.h`.
The order doesn't matter too much but the best place to add it is just before
**siril.ui**.

Plans
~~~~~
Short term: the build scripts will be changed to compile the UI files into a
GtkResource that lives within the Siril binary. This will reduce clutter on
users' filesystems and also be more space efficient, as the GtkResource is
compressed.

Longer term: currently some code still assumes that all of the UI is loaded all
at once at startup. The intention is to refactor this and then move to a
position where each dialog is loaded on demand the first time it is needed. This
should further improve startup time, at the cost of a small lag the first time
each dialog is opened (subsequent uses will be just as fast as at present).

Notes on editing UI files:

* Glade can now be problematic to use for editing existing UI files, as it will
  discard elements that are not available in the file. So if using a property
  that is defined in another UI file (*e.g.* one of the file filters) this will be
  discarded on save. Glade doesn't work at all with GTK4 anyway, so encouraging
  moving away from it is a good thing as we start to think about migrating to
  GTK4.
* In the short term Glade should still be okay for designing complex new
  GtkDialogs, however if you need to use elements defined in other UI files you
  will have to do some manual editing afterwards.

To edit UI files without relying on Glade, your options are:

* Write your UI as code. This is fine, but can be a bit painful for complex UIs.
* Write your UI as XML, by hand. As above, this is okay as long as you are handy
  with XML but can be painful for complex UIs.
* Continue to use Glade, but once you have finished the UI, run it through

  .. code-block:: bash

     gtk4-builder-tool validate [filename]
  
  This will force GTK4 compatibility but you may
  have some patching up to do at the end. This is probably what I'll do for
  complex dialogs, as even with the final patching up I think being able to use
  Glade makes the initial writing of the UI much easier.
* Cross your fingers that the GTK4 team realise how nuts it is not to have a
  graphical GUI editor, and provide one. Despite a couple of apparent efforts,
  nothing seems to be happening along these lines and the official recommendation
  from the GTK developers is to write your UI programatically or in hand-crafted
  XML!
