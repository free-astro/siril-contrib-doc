Region of Interest Processing
-----------------------------

Some operations support previewing only a Region of Interest (or ROI) in order
to speed up slower operations. This can work in two different ways:

* Some operations use the **live preview** method (*e.g.* Asinh Transform). Here,
  if the Preview check box is active then if a ROI is set only the ROI shows the
  preview; if no ROI is set the entire image is previewed.
* Other operations (*e.g.* Deconvolution) do not have a **live preview** method. If
  no ROI is set you can apply the operation to the entire image. If a ROI is set
  you have the choice of applying the operation to the entire image by clicking
  :guilabel:`Apply` or applying it only to the ROI, as an on-demand preview, by clicking
  :guilabel:`Preview in ROI`.
* As a graphical feature, ROI processing does not apply to Siril scripting
  commands.
* ROI processing does not apply to gemortry-changing operations.
* ROI processing does not apply to operations that require knowledge of the
  whole image to produce a consistent result (*e.g.* CLAHE).
* ROI processing does not apply to operations that combine multiple images (e.g.
  Star Recomposition).

How does it work?
~~~~~~~~~~~~~~~~~

This works by copying a chunk of gfit into gui.roi.fit and defining a bounding
rectangle (gui.roi.selection).

The image dialog has to do several things to set up for ROI processing. A good
example is available in :file:`src/filters/scnr.c` (there are more comments in this
file):

* Include :file:`gui/callbacks.h` and :file:`gui/siril_preview.h`
* In the dialog startup callback:

  * Call ``roi_supported(TRUE)`` to indicate that this dialog supports a ROI. The
    border of the ROI will be colored green.
  * You will also need to write a small callback that runs if the ROI is changed,
    created or deleted when the dialog is opened. The exemplar is ``scnr_roi_callback()``
    and in the dialog startup callback you should (a) run the callback, and (b)
    add it to the list using ``add_roi_callback(your_callback);``
  * Call ``copy_gfit_to_backup()``.
* At the start of the operation, call ``copy_gfit_to_backup()`` to ensure you are
  starting with a fresh copy of gfit. If a ROI is set, this also copies the ROI
  to a backup.
* The operation should work either on gfit (for working on the whole image) or
  gui.roi.fit (for working on the ROI only). This is often done by working on
  a ``fits* fit``, initialised something like ``fit = (previewing && gui.roi.active) ? &gui.roi.fit : &gfit;``
* You may find it easier to have two separate functions, ``myfunc_update_preview()``
  and ``myfunc_process_all()``. A good example of this can be seen in src/filters/asinh.c.
* At the end of the operation, if the operation is on the full image you must
  update the backup *and the ROI* by calling ``copy_gfit_to_backup() ; populate_roi();``
* When closing the dialog, you must:

  * Call ``roi_supported(FALSE)`` to indicate that ROI support is no longer available.
    The boorder of the ROI will turn back to red.
  * Call ``siril_preview_hide()``
  * Call ``remove_roi_callback(your_callback)`` to ensure that your callback no longer
    runs once the dialog is closed.

Typical symptoms that you have forgotten to do something are the ROI going a
different color on undo or when cancelling a ROI preview, etc.
