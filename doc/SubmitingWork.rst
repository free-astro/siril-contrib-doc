Submiting your work
--------------------

Contributions
=============

We encourage you, before to start implementing anything, to reach out to us by 
`filing a ticket in Gitlab <https://gitlab.com/free-astro/siril/-/issues/new>`_ 
with a **feature** tag. Explain what is your plan, if you plan to do it in multiple 
steps, anything that can help us figure out how it fits within the current workload, 
if we will be available to help/test/review, what is the appropriate milestone etc...

If we are in a release cycle (beta/RC*), it will surely have to wait for the 
next big release. While we don't enforce a strict code-freeze policy, we avoid 
pushing new features once we try to reach a stable state. But nonetheless, your 
feature will make its way into Siril!

Once the plan is clear, you'll need to:

- Fork this project in your own repo
- Create a branch (do not make your changes on your fork master!!!) and make your 
  changes
- Create a Merge Request from your branch to master, with a **Draft:** prefix. 
  Please keep the box :guilabel:`Allow commits from members who can merge to the 
  target branch` ticked. This will allow us to make commit in your branch if 
  required.
- Once the work has progressed enough, follow the steps 
  `there <https://gitlab.com/free-astro/siril-doc/-/blob/main/README.md#documenting-a-new-feature>`_ 
  so that your feature is properly documented.
- Test your feature extensively. As you have seen in the introduction of 
  :ref:`Writing a Feature <writingfeature:writing a feature>`, there are many 
  cases to cover!
- When you think it is ready, you can remove the **Draft:** prefix.
- We will first test but most importantly review your code. While we've tried to 
  give you here a good overview of what's going on behind the curtains, we may 
  find stuff in your code that does not belong where it should or that could do 
  with a bit of rewriting.
- Once all the commenting threads have been resolved, we can merge your feature! 
  Congrats!

Checklist
=========

Here-below a list of what you should check:

- you have updated files :file:`ChangeLog` and :file:`NEWS` explaining (in short) 
  what your new feature or bugfix does,
- your fork master branch is synced with Siril master and you have no merge 
  conflict (remember to merge master in your branch regularly),
- you have changes in the :ref:`4 command files <importantfiles:command related files>` 
  (if you have added/updated a command),
- you have added new files in :file:`po/POTFILES.in` if they have translatables,
- you don't have **ANY** GUI-related calls in the processing threads,
- you have checked that no job has failed on Gitlab CI.

Tips and tricks
===============

In no particular order, yet all equally important:

- comment your code
- keep all the functions that can be static static
- do not define a variable with a scope broader than required and don't forget 
  to initialize it if needed
- handle error conditions and give proper indications of what went wrong
- if you need to write something more than once, it means you should make a 
  function out of it. The parser in :ref:`WritingFeature:Write the command for 
  a single image` is an example of this.
- if you need to run something more than once, you should cache the result. 
  Siril is the fastest astronomy image processing software and we work hard to 
  keep it this way.
- all the variables that are allocated with glib functions should be freed with 
  ``g_free``. Conversely, use ``free`` on memory allocated with ``malloc``/``calloc``.
- use the functions provided by ``glib`` to handle creating filenames/paths. 
  They do prevent lots of troubles for multi-platform software.
- all the commands should be made stateless, meaning that they should not
  require a previous command to be successful (other than loading an image for 
  single-image commands) and will give always the same result for the same input
- For some tricky cases, you may need to use ``#ifdef _WIN32`` to handle 
  behavior on Windows and the opposite for Unix-based OSes.
- if you need to import ``<windows.h>`` in one of your functions, make sure 
  :file:`core/siril.h` is included after. Otherwise, it will break Windows build.
- if in doubt, reach out to us to know if a function already exists that does 
  what you want. It could be hidden there, awaiting to be rediscovered.
- do not add depencies to other libs before having discussed the matter with us. 
  We are trying to keep the number of deps as low as possible.

Bug fixes
=========

You may also want to contribute by fixing a bug described in a `ticket <https://gitlab.com/free-astro/siril/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Bug>`_.  

.. note::
    If you spot a bug, first thing is to open a ticket anyway.

You will first need to determine if it affects both stable branch and developement branch (master), if not already mentionned in the ticket.
We will use an example where stable is branch `1.2` and next major release is `1.4` (under master)

**If it affects both:**

- tag ticket it to milestone `1.2.0`.
- Create a branch with your fix and make a MR. Your MR should target **master**.
- Update changelog/NEWS adding the description of the fix and the ticket number (#xxx) in the 1.2.x section
- Remove the Draft: status
- Once we have merged to master, thanks to your tagging it to `1.2.0`, we will know we need to backport and we'll cherry-pick in `1.2` branch directly.

**If it affects only 1.2:**

- same as above but your MR should target `1.2` branch

**It it affects only master:**

- same but you should tag it to `1.4.0` milestone

.. tip::

    In the MR description, don't forget to add a mention like `fixes #xxx`. Gitlab will recognize the syntax and `close the ticket automatically <ttps://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically>`_ when it gets merged.  


