Global Variables
----------------

There are a few global variables worth knowing before starting.

gfit
====
This is a ``fits`` structure (see definition in :file:`siril.h`), which is the 
loaded image.

com
===
This a ``cominfo`` structure (see definition in :file:`siril.h`), which enables 
to share information across different processes.

.. warning::

    You should avoid by all means adding new members to this structure, unless 
    there is no obvious way to do otherwise.

    For single feature variables that must live longer than a process, it is 
    possible to declare them in static in the
    concerned file, emulating class variables from object-oriented programming.

gui
===

This a ``guiinfo`` structure (see definition in :file:`siril.h`), which enables 
to share gui information across different processes.

