Important Files
---------------

This page takes you through the important files to know.

src/core
========

This folder contains all the files defining the core definitions and functions 
of Siril.

siril.h
*******

This is the file where all the constants, enums and types that are shared across 
most functions are defined. As such, it is included in most files. You should 
definetely have a look there before doing anything else.

You should always question yourself before changing anything there. Meaning, 
"does my structure or type need to be defined up there?" or can it be defined 
locally (in my feature).


settings.h and settings.c
*************************
This is where the settings and preferences are initialized. Unless starting from 
a fresh install (Siril will then create a default), preferences are loaded from 
an :file:`.ini` file everytime Siril is launched. Some values which are not 
defined in the preferences are also intialized here.

Everytime you will need to add something there, you will need to question 
yourself: Do I want this value to be defined by the user or do I want to start 
from a good default value? If the answer is the first option, then it needs to 
be **persistant**, otherwise it is **volatile**.

For instance, Home directory or language is clearly a persistant user preference 
while the default values to start a dynamic PSF (those defined in 
``star_finder_params``) must be kept volatile. Storing exotic values for dynamic 
PSF parameters may get the registration to fail, so we don't want to keep them.

:file:`settings.h` defines the necessary enums and ``*_config`` structures 
and the preferences structure ``pref_struct`` that gathers them all. It is also 
used to declare the useful functions to manipulate this structure.

settings.c is used to:

- define the default values for the full preference structure
- define the values that will be made persistent, meaning that will get written 
  in the :file:`.ini` file. If you need to have persistent settings, declare 
  them in the ``all_settings`` structure.

command related files
*********************

These files are discussed at length in :ref:`this section <writingfeature:write 
the command for a single image>`. They are basically all the files used to define 
commands:

- :file:`command.c` is the file where the arguments parsing is done and where the calcs 
  are effectively launched.
- :file:`command.h` is used to define all the command functions and also the enum to 
  catch errors.
- :file:`command_def.h` is the file where the tooltip for every command is written.
- :file:`command_list.h` defines each command and its metadata along with necessary 
  links to the other files.

.. tip::

    Everytime you write a new command, it is good to check that there are 
    changes in these 4 files. Otherwise, it means you have missed something.


processing.c
************

This file, or at least how to use the function its contains, is discussed at 
length in :ref:`this section <writingfeature:write the command for a sequence>`. 
This is basically where you will find all the machinery to process sequences in 
a generic manner.

.. warning::

    It is not expected that you need to make changes in there

siril_date.c and siril_world_cs.c
*********************************

These 2 files contain lots of useful functions to manipulate dates and WCS info.

utils.c
*******

This file contains utilities to:

- convert numeric types
- manipulate strings
- manipulate filenames

src/gui
=======

If the feature you are writing has a GUI version, you should create a file in 
this folder, with the same name as your main feature file, which will contain 
all its callbacks and GUI-related helper functions.

UI files
********

The UI is defined by the UI files in :file:`src/gui/ui_files`. See the 
:ref:`GUI <GUI:Graphical User Interface (GUI)>` page for more details.

image_display.c
***************

This file contains all the functions that are used to draw the image and 
overlays, see for instance ``draw_stars`` or ``draw_annotates``. These functions 
are only called in `idle functions <https://developer-old.gnome.org/gdk3/3.24/gdk3-Threads.html>`_ 
and when Siril is launched in GUI mode (as opposed to CLI).

image_interactions.c
********************
This file contains all the callbacks to capture mouse interactions with the 
loaded image.


src/io
======

This folder contains all the files and functions to read/write files:

- :file:`image_format_fits.c`: to deal with FITS images, the natve format used 
  by Siril
- :file:`fits_sequence.c`: to deal with fitseq, a multiple-image container 
  (same as FITS but with one more dimension)
- :file:`ser.c`: to deal with `ser files <http://www.grischa-hahn.homepage.t-online.de/astro/ser/>`_, 
  a multiple-image container used for planetary images
- :file:`seqfile.c`: to read/write ``.seq`` files
- :file:`sequence.c`: to deal with sequences in Siril

There are more files in there but these are the important ones to begin with.

po/
===

POTFILES.in
***********

This files lists all the files that contain translatable strings.

.. tip::

    Everytime you create a new file, if it logs new strings in the Console or 
    if it is a new UI file, it should be added there (respecting the alphabetical 
    order!).
