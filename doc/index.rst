Welcome to Siril documentation for contributors
===============================================

You will hopefully find here useful information to start contributing to Siril.

You should read the different section in the order they are displayed on the
left bar. Do not miss the coding recommendations at :ref:`the end
<submitingwork:tips and tricks>`.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contents:

   ImportantFiles
   GlobalVariables
   WritingFeature
   MultiThreading
   GUI
   ROI_processing
   SubmitingWork
   CIbuilds
   UsefulStuff

