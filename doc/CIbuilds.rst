CI/CD builds
============


Windows builds
--------------

Build environment
+++++++++++++++++

Windows builds are made through `msys2 <https://www.msys2.org/>`_ platform, using
mingw64 environment. As this platform is not readily available on Gitlab shared
Windows `runners <https://docs.gitlab.com/ee/ci/runners/hosted_runners/windows.html>`_,
it needs to be installed on the runner.

This can be a lengthy process (it is equivalent to reinstalling an OS for every job).
We also need to re-install all the dependencies before actually building Siril.
In order to shorten this process, another project has been set up, namely 
`siril-windows <https://gitlab.com/free-astro/siril_windows>`_.

This also freezes the versions of all packages so that we can use a previous image
as a fallback in case a new version of a apackage breaks the build.

This project basically:
- install msys2/ming64 environment
- install all the dependencies
- creates an archive in Free-astro registries to be used by Siril Windows native CI job

The list of dependencies to be installed is fetched from `siril-deps.sh <https://gitlab.com/free-astro/siril/-/blob/master/build/windows/native-gitlab-ci/siril-deps.sh>`_
so that any update to this file should call for creating a new image archive.

.. note::
    opencv package is not installed as is, as the default build from msys2 
    includes too many optional packages. A dedicated minimal build is made with 
    only the required modules. Should a new module be required, the PKGBUILD file
    would need updating.


Creating a new image
++++++++++++++++++++

This is required when we want to update msys2/mingw64 environment to have newer versions,
or if we need to add a new dependency.

Goto to siril-windows repo and modify .gitlab-ci.yml variable:

.. code-block:: bash

    MSYS2INSTALLDEPS_REF: 3eb3628c71c04944dd17651ac84936a1a6b0b8fd

The commit SHA should be the one of from Siril repo with the relevant siril-deps.sh.
Committing this change will update once $CI_PIPELINE_IID.

Check the current ``$CI_PIPELINE_IID`` (you can show Pipeline IID in this `page <https://gitlab.com/free-astro/siril_windows/-/pipelines>`_).
No need to let the run complete, you can cancel it.

Now tag your last commit (pushing the tag will update IID once more)
using the name ``r($CI_PIPELINE_IID + 1)`` and push.

Once the pipeline completes, it will release the image archive to the project
`registry <https://gitlab.com/groups/free-astro/-/packages>`_.

Now in Siril .gitlab-ci.yml file, update the following variable:

.. code-block:: bash

    SDW_VERSION: 30

The number of the version should match the value of the tag created just above.

Other builds
------------

HOLD